﻿namespace BowlingKataCSharp
{
    public class TurnScore
    {
        public string NextTurns { get; }
        public int Score { get; }

        public TurnScore(string nextTurns, int score)
        {
            NextTurns = nextTurns;
            Score = score;
        }
    }
}