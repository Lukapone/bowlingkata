﻿using System;
using System.Runtime.Remoting.Messaging;
using NUnit.Framework.Constraints;

namespace BowlingKataCSharp
{
    public class BowlingCalculator
    {
        public int CalculateScore(string input)
        {
            if (input.Length < 2) { return 0;}
            if (input.Length < 3) { return GetValueForSymbol(input[0]) + GetValueForSymbol(input[1]); }
            if (input.Length == 3 && input[0].Equals('X'))
            {
                return CalculateScoreForThreeThrows(input[0], input[1], input[2]).Score;
            }
            var turn = CalculateScoreForThreeThrows(input[0], input[1], input[2]);
            return turn.Score + CalculateScore($"{turn.NextTurns}{input.Substring(3)}");
        }

        public TurnScore CalculateScoreForThreeThrows(char throw0, char throw1, char throw2)
        {
            if (throw0.Equals('X'))
            {
                return new TurnScore(
                    nextTurns: $"{throw1}{throw2}", 
                    score: 10 + SumStrike(throw1, throw2)
                    );
            }

            if (throw1.Equals('/'))
            {
                return new TurnScore(
                    nextTurns: $"{throw2}",
                    score: 10 + GetValueForSymbol(throw2)
                );
            }
            return new TurnScore(
                nextTurns: $"{throw2}",
                score: GetValueForSymbol(throw0) + GetValueForSymbol(throw1)
            );
        }

        public int SumStrike(char throwSymbol1, char throwSymbol2)
        {
            if (throwSymbol2.Equals('/'))
            {
                return 10;
            }

            return GetValueForSymbol(throwSymbol1) + GetValueForSymbol(throwSymbol2);
        }


        public int GetValueForSymbol(char symbol)
        {
            if (symbol.Equals('-'))
            {
                return 0;
            }
            if (symbol.Equals('X'))
            {
                return 10;
            }
            return int.Parse(symbol.ToString());
        }

    }
}