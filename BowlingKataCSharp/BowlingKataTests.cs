﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

//problem url: http://codingdojo.org/kata/Bowling/
namespace BowlingKataCSharp
{
    [TestFixture]
    public class BowlingKataTests
    {
        private BowlingCalculator _sut;


        [SetUp]
        public void TestSetup()
        {
            _sut = new BowlingCalculator();
        }



        [Test]
        public void SimpleStrikesOf2_2Return4ForScore()
        {
            var input = "22";
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(4));

        }
        [Test]
        public void SpareStrikesOf2_Spare_2Return12ForScore()
        {
            var input = "2/2";
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(12));

        }
        [Test]
        public void AllStrikes12()
        {
            var input = "XXXXXXXXXXXX";
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(300));

        }

        [Test]
        public void All0And9_20Rolls()
        {
            var input = "-9-9-9-9-9-9-9-9-9-9";
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(90));

        }

        [Test]
        public void All5Spare_21Rolls()
        {
            var input = "5/5/5/5/5/5/5/5/5/5/5";
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(150));

        }
        [Test]
        public void AllRandom()
        {
            var input = "125/X45187154--51X22";
                        //3+20+19+9+9+8+9+0+6+14
            var score = _sut.CalculateScore(input);

            Assert.That(score, Is.EqualTo(97));

        }
    }

}
