#include "BowlingCalculator.h"

int BowlingCalculator::CalculateScore(std::string input) const
{
	if (input.size() < 2) { return 0; }
	if (input.size() < 3) { return GetValueForSymbol(input[0]) + GetValueForSymbol(input[1]); }
	if (input.size() == 3 && input[0] == 'X')
	{
		return CalculateScoreForThreeThrows(input[0], input[1], input[2]).Score();
	}
	auto turn = CalculateScoreForThreeThrows(input[0], input[1], input[2]);
	return turn.Score() + CalculateScore(turn.NextTurn() + input.substr(3));
}

TurnScore BowlingCalculator::CalculateScoreForThreeThrows(char throw0, char throw1, char throw2)const
{
	if (throw0 == 'X')
	{
		return TurnScore(
			std::string{throw1,throw2},
			10 + SumStrike(throw1, throw2)
		);
	}
	if (throw1 ==  '/')
	{
		return TurnScore(
			std::string{ throw2 },
			10 + GetValueForSymbol(throw2)
		);
	}
	return TurnScore(
		std::string{ throw2 },
		GetValueForSymbol(throw0) + GetValueForSymbol(throw1)
	);
}

int BowlingCalculator::SumStrike(char symbol1, char symbol2)const
{
	if (symbol2 == '/')
	{
		return 10;
	}

	return GetValueForSymbol(symbol1) + GetValueForSymbol(symbol2);
}

int BowlingCalculator::GetValueForSymbol(char symbol)const
{
	if (symbol == '-')
	{
		return 0;
	}
	if (symbol == 'X')
	{
		return 10;
	}
	return symbol - '0';
}
