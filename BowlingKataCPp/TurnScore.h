#pragma once
#include <string>


class TurnScore {
private:
	std::string nextTurn;
	int score;

public:
	TurnScore(std::string nextTurn, int score);
	std::string NextTurn();
	int Score();
};