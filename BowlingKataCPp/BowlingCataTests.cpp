#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BowlingCalculator.h"


TEST_CASE("Bowling calculator calculate correct score for different inputs") {

	auto _sut = BowlingCalculator();

	SECTION("Simple turn with 2 and 2 will return score of 4") {
		REQUIRE(_sut.CalculateScore("22") == 4);
	}
	SECTION("Normal 2 spare and normal 2 will return 12") {
		REQUIRE(_sut.CalculateScore("2/2") == 12);
	}
	SECTION("All strikes") {
		REQUIRE(_sut.CalculateScore("XXXXXXXXXXXX") == 300);
	}
	SECTION("All 0 and 9 20 rolls") {
		REQUIRE(_sut.CalculateScore("-9-9-9-9-9-9-9-9-9-9") == 90);
	}
	SECTION("All 5 and spare 21 rolls") {
		REQUIRE(_sut.CalculateScore("5/5/5/5/5/5/5/5/5/5/5") == 150);
	}
	SECTION("All random") {
		REQUIRE(_sut.CalculateScore("125/X45187154--51X22") == 97);
	}

}

