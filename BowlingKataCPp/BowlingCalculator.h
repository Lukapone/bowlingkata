#pragma once
#include <string>
#include "TurnScore.h"

class BowlingCalculator {

public:
	int CalculateScore(std::string input) const;
	TurnScore CalculateScoreForThreeThrows(char throw0, char throw1, char throw2)const;
	int SumStrike(char symbol1, char symbol2)const;
	int GetValueForSymbol(char symbol)const;

};