#include "TurnScore.h"

TurnScore::TurnScore(std::string nextTurn, int score): nextTurn(std::move(nextTurn)), score(score)
{
}

std::string TurnScore::NextTurn()
{
	return nextTurn;
}

int TurnScore::Score()
{
	return score;
}
